package co.extreme.cliente.model;

import lombok.*;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Table(name = "pqr")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Entity
public class Pqr implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Basic
    @Column(name = "tipo")
    private int tipo;

    @Basic
    @Column(name = "asunto")
    private String asunto;

    @ManyToOne
    @JoinColumn(name = "id_usuario", referencedColumnName = "id", nullable = false)
    private Usuarios idUsuario;


    @Basic
    @Column(name = "estado")
    private int estado;

    @Basic
    @Column(name = "fecha_creacion")
    private Date fechaCreacion;

    @Basic
    @Column(name = "fecha_limite")
    private Date fechaLimite;


}