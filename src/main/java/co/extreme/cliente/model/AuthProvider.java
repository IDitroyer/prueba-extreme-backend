package co.extreme.cliente.model;

public enum  AuthProvider {
    local,
    facebook,
    google,
    github
}
