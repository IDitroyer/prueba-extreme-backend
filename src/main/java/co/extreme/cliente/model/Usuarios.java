package co.extreme.cliente.model;

import lombok.*;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;



@Table(name = "usuarios")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Entity
public class Usuarios implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Basic
    @Column(name = "nombre")
    private String nombre;
    @Basic
    @Column(name = "usuario")
    private String usuario;
    @Basic
    @Column(name = "password")
    private String password;
    @Basic
    @Column(name = "correo")
    private String correo;
    @Basic
    @Column(name = "celular")
    private String celular;
    @Basic
    @Column(name = "identificacion")
    private String identificacion;
    @Basic
    @NotNull
    @Column(name = "provider")
    private AuthProvider provider;
    @Size(max = 255)
    @Column(name = "provider_id")
    private String providerId;

    @Column(name = "activo")
    private Boolean activo;

    @ManyToOne(fetch = FetchType.EAGER, cascade = { CascadeType.MERGE, CascadeType.REFRESH })
    @JoinTable(name = "usuarios_roles", joinColumns = @JoinColumn(name = "usuario_id"), inverseJoinColumns = @JoinColumn(name = "roles_id"))
    private Roles roles;


}
