package co.extreme.cliente.controller;

import co.extreme.cliente.model.Usuarios;
import co.extreme.cliente.payload.AuthResponse;
import co.extreme.cliente.payload.LoginRequest;

import co.extreme.cliente.security.TokenProvider;


import co.extreme.cliente.service.UsuariosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.*;

import org.springframework.http.HttpStatus;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private TokenProvider tokenProvider;




    @Autowired
    private UsuariosService usuariosService;

    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        Map<String, Object> response = new HashMap<>();
        try{
            Optional<Usuarios> usuario = usuariosService.findByUsuario(loginRequest.getUsuario());

            if(usuario.isPresent()){

                if (usuario.get().getActivo() == false ) {
                    response.put("message", "Su usuario se encuentra inactivo, contacte con su proveedor de servicios ");
                    return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
                }


                Authentication authentication = authenticationManager.authenticate(
                        new UsernamePasswordAuthenticationToken(
                                loginRequest.getUsuario(),
                                loginRequest.getPassword()
                        )
                );

                SecurityContextHolder.getContext().setAuthentication(authentication);

                String token = tokenProvider.createToken(authentication);
                return ResponseEntity.ok(new AuthResponse(token));
            }
        }catch(Exception e){
            response.put("message", "Credenciales incorrectasl, intente de nuevo");
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }


        response.put("message", "Revise las credenciales de acceso");
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }




}
