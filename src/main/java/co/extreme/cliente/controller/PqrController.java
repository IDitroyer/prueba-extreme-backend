package co.extreme.cliente.controller;

import co.extreme.cliente.model.Pqr;
import co.extreme.cliente.security.CurrentUser;
import co.extreme.cliente.security.UserPrincipal;
import co.extreme.cliente.service.PqrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = {"*"})
@RequestMapping("/api/v1")
@RestController
public class PqrController {

    @Autowired
    private PqrService service;

    @GetMapping("/pqr/list")
    public List<Pqr> findAll() {
        return service.findAll();
    }


    @PostMapping("/pqr/save")
    public ResponseEntity<?> save(@RequestBody Pqr pqr) {
        Map<String, Object> response = new HashMap<>();
        try {
            response.put("pqr", service.save(pqr));
        } catch (DataAccessException e) {
            response.put("message", "fallo al guardar el pqr");
            response.put("error", e.getMessage()+": "+e.getMostSpecificCause().getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping("/pqr/usuario")
    public List<Pqr> getByUser(@CurrentUser UserPrincipal principal) {
        return service.findByUsuario(principal.getId());
    }

    @GetMapping("/pqr/usuario/{id}")
    public List<Pqr> findByUsuario(@PathVariable("id") int id) {
        return service.findByUsuario(id);
    }



    @DeleteMapping("/pqr/delete/{id}")
    public ResponseEntity<?> deleteParametros(@PathVariable("id") final Integer id) {

        Map<String, Object> response = new HashMap<>();
        try {
            service.deleteById(id);
            response.put("mesaage", "El pqr ha sido eliminado");
        } catch (DataAccessException e) {
            response.put("message", "fallo al eliminar el pqr");
            response.put("error", e.getMessage()+": "+e.getMostSpecificCause().getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }



}
