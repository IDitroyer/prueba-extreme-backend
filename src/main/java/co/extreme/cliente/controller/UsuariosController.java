package co.extreme.cliente.controller;

import co.extreme.cliente.config.AppProperties;
import co.extreme.cliente.model.AuthProvider;
import co.extreme.cliente.model.Usuarios;
import co.extreme.cliente.security.TokenProvider;

import co.extreme.cliente.service.UsuariosService;
import lombok.*;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;


@Setter
@Getter
@Data
@NoArgsConstructor
@AllArgsConstructor
@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/api/v1/")
public class UsuariosController {


    @Autowired
    private UsuariosService service;

    @Autowired
    private TokenProvider jwtTokenUtil;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private static final Logger logger = LoggerFactory.getLogger(TokenProvider.class);

    private AppProperties appProperties;


    public UsuariosController(AppProperties appProperties) {
        this.appProperties = appProperties;
    }

    @GetMapping("/usuarios/list")
    public List<Map<String, Object>> findAllUsuarios() {
        return service.findAllUsuarios();
    }

    @PostMapping("/usuarios/crear")
    public ResponseEntity<?> crear(@RequestBody Usuarios usuario) {
        Map<String, Object> response = new HashMap<>();
        Usuarios body = usuario;
        body.setPassword(passwordEncoder.encode(usuario.getPassword()).toString());
        body.setProvider(AuthProvider.local);
        body.setActivo(true);

        Usuarios res = service.save(body);
        System.out.println(res.getUsuario());
        service.setUserRol(res.getId());

        response.put("message", "El usuario ha sido creado correctamente");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }





    @PostMapping("/usuarios/custom/save")
    public ResponseEntity<?> customSave(@RequestBody Usuarios usuario) {

        Map<String, Object> response = new HashMap<>();
        Optional<Usuarios> old = service.findById(usuario.getId());
        Usuarios user = old.get();

        user.setUsuario(usuario.getUsuario());
        user.setCorreo(usuario.getCorreo());
        user.setNombre(usuario.getNombre());
        user.setActivo(usuario.getActivo());
        user.setCelular(usuario.getCelular());
        user.setIdentificacion(user.getIdentificacion());
        service.save(user);
        response.put("message", "El usuario se ha guardado correctamente");
        return new ResponseEntity<>(response, HttpStatus.OK);


    }


}
