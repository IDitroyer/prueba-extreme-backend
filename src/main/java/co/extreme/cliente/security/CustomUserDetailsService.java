package co.extreme.cliente.security;


import co.extreme.cliente.exception.ResourceNotFoundException;
import co.extreme.cliente.model.Usuarios;
import co.extreme.cliente.repository.UsuariosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;



@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    UsuariosRepository usuariosRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String usuario)
            throws UsernameNotFoundException {
        Usuarios user = usuariosRepository.findByUsuario(usuario)
                .orElseThrow(() ->
                        new UsernameNotFoundException("User not found with email : " + usuario)
        );

        return UserPrincipal.create(user);
    }

    @Transactional
    public UserDetails loadUserById(Integer id) {
        Usuarios user =  usuariosRepository.findById(id).orElseThrow(
            () -> new ResourceNotFoundException("User", "id", id)
        );

        return UserPrincipal.create(user);
    }
}