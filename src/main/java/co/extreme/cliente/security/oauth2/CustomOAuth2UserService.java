package co.extreme.cliente.security.oauth2;

import co.extreme.cliente.exception.OAuth2AuthenticationProcessingException;
import co.extreme.cliente.model.AuthProvider;
import co.extreme.cliente.model.Usuarios;
import co.extreme.cliente.repository.UsuariosRepository;
import co.extreme.cliente.security.UserPrincipal;
import co.extreme.cliente.security.oauth2.user.OAuth2UserInfo;
import co.extreme.cliente.security.oauth2.user.OAuth2UserInfoFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Calendar;
import java.util.Optional;

@Service
public class CustomOAuth2UserService extends DefaultOAuth2UserService {

    @Autowired
    private UsuariosRepository usuariosRepository;

    @Override
    public OAuth2User loadUser(OAuth2UserRequest oAuth2UserRequest) throws OAuth2AuthenticationException {
        OAuth2User oAuth2User = super.loadUser(oAuth2UserRequest);

        try {
            return processOAuth2User(oAuth2UserRequest, oAuth2User);
        } catch (AuthenticationException ex) {
            throw ex;
        } catch (Exception ex) {
            // Throwing an instance of AuthenticationException will trigger the OAuth2AuthenticationFailureHandler
            throw new InternalAuthenticationServiceException(ex.getMessage(), ex.getCause());
        }
    }

    private OAuth2User processOAuth2User(OAuth2UserRequest oAuth2UserRequest, OAuth2User oAuth2User) {
        OAuth2UserInfo oAuth2UserInfo = OAuth2UserInfoFactory.getOAuth2UserInfo(oAuth2UserRequest.getClientRegistration().getRegistrationId(), oAuth2User.getAttributes());
        if(StringUtils.isEmpty(oAuth2UserInfo.getUsuario())) {
            throw new OAuth2AuthenticationProcessingException("Email not found from OAuth2 provider");
        }

        Optional<Usuarios> userOptional = usuariosRepository.findByUsuario(oAuth2UserInfo.getUsuario());
        Usuarios user;
        if(userOptional.isPresent()) {
            user = userOptional.get();
            System.out.println(user);
            if(!user.getProvider().equals(AuthProvider.valueOf(oAuth2UserRequest.getClientRegistration().getRegistrationId()))) {
                throw new OAuth2AuthenticationProcessingException("Estas intentando iniciar sesion con una cuenta " +
                        user.getProvider() + ". Por favor usa tu cuenta " + user.getProvider());
            }
            user = updateExistingUser(user, oAuth2UserInfo);
        } else {
            user = registerNewUser(oAuth2UserRequest, oAuth2UserInfo);
        }

        return UserPrincipal.create(user, oAuth2User.getAttributes());
    }

    private Usuarios registerNewUser(OAuth2UserRequest oAuth2UserRequest, OAuth2UserInfo oAuth2UserInfo) {
        Usuarios user = new Usuarios();


        
        user.setProvider(AuthProvider.valueOf(oAuth2UserRequest.getClientRegistration().getRegistrationId()));
        user.setProviderId(oAuth2UserInfo.getId());

        user.setNombre(oAuth2UserInfo.getName());
        user.setUsuario(oAuth2UserInfo.getUsuario());

        java.sql.Time time = new java.sql.Time(Calendar.getInstance().getTime().getTime());


        return usuariosRepository.save(user);
    }

    private Usuarios updateExistingUser(Usuarios existingUser, OAuth2UserInfo oAuth2UserInfo) {


        return usuariosRepository.save(existingUser);
    }

}
