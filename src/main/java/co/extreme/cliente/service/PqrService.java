package co.extreme.cliente.service;

import co.extreme.cliente.model.Pqr;
import co.extreme.cliente.repository.PqrRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;


@Service
public class PqrService {

    @Autowired
    PqrRepository pqrRepository;


    public List<Pqr> findAll() {
        return pqrRepository.findAll();
    }

    public Optional<Pqr> findById(int id) {return pqrRepository.findById(id);}

    public Pqr save(Pqr pqr) {

        if(pqr.getFechaCreacion() == null) {
            pqr.setFechaCreacion(new Date());
        }

        if (pqr.getFechaLimite() == null) {
            Integer dias = 0;
            switch (pqr.getEstado()){
                case 1 :
                    dias = 7;
                    break;
                case 2 :
                    dias = 3;
                    break;
                case 3 :
                    dias = 2;
                    break;
            }
            Date today = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(today);
            cal.add(Calendar.DAY_OF_MONTH, dias);
            Date modifiedDate = cal.getTime();

            pqr.setFechaLimite(modifiedDate);
        }



        return pqrRepository.save(pqr);
    }


    public void deleteById(Integer id) {
        pqrRepository.deleteById(id);
    }

    public List<Pqr> findByUsuario(Integer id) {return pqrRepository.findByUsuario(id);}


}
