package co.extreme.cliente.service;

import co.extreme.cliente.model.Usuarios;
import co.extreme.cliente.repository.UsuariosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Transactional
public class UsuariosService {

    @Autowired
    private UsuariosRepository usuariosRepository;

    public List<Map<String, Object>> findAllUsuarios() {return usuariosRepository.findAllUsuarios();}

    public Optional<Usuarios> findByUsuario(String usuario){
        return usuariosRepository.findByUsuario(usuario);
    }

    public Optional<Usuarios> findById(Integer id) {return usuariosRepository.findById(id); }

    public Usuarios save(Usuarios usuarios) {
        return usuariosRepository.save(usuarios);
    }

    public void setUserRol(int id) { usuariosRepository.setUserRol(id); }
}
