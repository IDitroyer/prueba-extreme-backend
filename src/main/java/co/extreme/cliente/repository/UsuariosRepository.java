package co.extreme.cliente.repository;

import co.extreme.cliente.model.Usuarios;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface UsuariosRepository extends JpaRepository<Usuarios, Integer> {


    Optional<Usuarios> findByUsuario(String usuario);

    @Query("select u.id as id, u.nombre as nombre, u.identificacion as identificacion, u.correo as correo, u.celular as celular, u.usuario as usuario, u.activo as activo, (select count(p) from Pqr p where p.idUsuario.id = u.id) as pqr from Usuarios u where u.roles.id = 2")
    List<Map<String, Object>> findAllUsuarios();

    @Modifying
    @Transactional
    @Query(value = "INSERT INTO usuarios_roles (usuario_id, roles_id )\n" +
            "VALUES (:id, 2);", nativeQuery = true)
    void setUserRol(@Param("id") int id);

}
