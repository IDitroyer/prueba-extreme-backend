package co.extreme.cliente.repository;


import co.extreme.cliente.model.Pqr;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.List;


@Repository
public interface PqrRepository extends JpaRepository<Pqr, Integer> {

    List<Pqr> findAll();

    @Query("select p from Pqr p where p.idUsuario.id = :id")
    List<Pqr> findByUsuario(@Param("id") int id);

}
