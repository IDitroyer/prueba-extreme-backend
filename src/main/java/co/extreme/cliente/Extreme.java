package co.extreme.cliente;

import co.extreme.cliente.config.AppProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
@EnableConfigurationProperties(AppProperties.class)
@EnableScheduling
public class Extreme implements CommandLineRunner {
    
    
    @Autowired
    private PasswordEncoder passwordEncoder;
    
    


	public static void main(String[] args) {
		SpringApplication.run(Extreme.class, args);
	}

    @Override
    public void run(String... args) throws Exception {

        System.out.println(passwordEncoder.encode("123456789").toString());

        System.out.println("CREATED AND INTEGRATED BY - 2021 - JESUS QUINTO");
      



    }
}
