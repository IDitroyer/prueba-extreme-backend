# prueba-extreme-backend

Aplicativo backend realizado en Spring-boot con una base de datos Mysql

## La base de datos

Esta desarrollada en Mysql, en este repositorio encontrará un archivo llamado esquema mysql con la estructura de las tablas e insersion de datos basicas y necesarias como roles y usuarios, la configuración de la base de datos es configurable en el archivo yml del proyecto

## Config en el yml

Los datos referentes a la conexion dependen del servidor local del revisor, ejemplo :
        username: root
        password: Admin123
        url: jdbc:mysql://localhost:3306/prueba_extreme?verifyServerCertificate=false&useSSL=false&equireSSL=falseuseJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=America/Bogota

## auditorias

La tabla de auditorias compete el registro de acciones en la tabla de pqrs, los triggers están en el archivo sql previamente mencionado



