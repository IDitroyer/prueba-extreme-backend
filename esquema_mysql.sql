create table auditoria
(
    id          int auto_increment,
    fecha       datetime     null,
    usuario_web varchar(255) null,
    usuario_bd  varchar(255) null,
    accion      text         null,
    constraint auditoria_id_uindex
        unique (id)
);

alter table auditoria
    add primary key (id);

create table pqr
(
    id             int auto_increment,
    tipo           int  null comment '1 - peticion
2 - queja 
3 - reclamo',
    asunto         text null,
    id_usuario     int  null,
    estado         int  null comment '1 - nuevo
2 - ejecucion
3 - cerrado',
    fecha_creacion date null,
    fecha_limite   date null,
    constraint pqr_id_uindex
        unique (id)
);

alter table pqr
    add primary key (id);

create definer = root@localhost trigger pqr_user_delete
    after delete
    on pqr
    for each row
BEGIN
    INSERT INTO auditoria (fecha, usuario_web, usuario_bd, accion)
    VALUES (CURRENT_DATE(), OLD.id_usuario, SYSTEM_USER(), 'DELETE PQR');
END;

create definer = root@localhost trigger pqr_user_insert
    after insert
    on pqr
    for each row
BEGIN
    INSERT INTO auditoria (fecha, usuario_web, usuario_bd, accion)
    VALUES (CURRENT_DATE(), NEW.id_usuario, SYSTEM_USER(), 'INSERT PQR');
END;

create definer = root@localhost trigger pqr_user_update
    after update
    on pqr
    for each row
BEGIN
    INSERT INTO auditoria (fecha, usuario_web, usuario_bd, accion)
    VALUES (CURRENT_DATE(), NEW.id_usuario, SYSTEM_USER(), 'UPDATE PQR');
END;

create table roles
(
    id     int auto_increment,
    nombre varchar(20) null,
    constraint UK_ldv0v52e0udsh2h1rs0r0gw1n
        unique (nombre),
    constraint roles_id_uindex
        unique (id)
)
    engine = MyISAM;

alter table roles
    add primary key (id);

create table usuarios
(
    id             int auto_increment,
    celular        varchar(255) null,
    correo         varchar(255) null,
    identificacion varchar(255) null,
    nombre         varchar(255) null,
    password       varchar(255) null,
    provider       int          null,
    activo         int          null comment '0 = false
1 = true',
    usuario        varchar(255) null,
    provider_id    varchar(255) null,
    constraint usuarios_id_uindex
        unique (id)
)
    engine = MyISAM;

alter table usuarios
    add primary key (id);

create table usuarios_roles
(
    usuario_id int not null,
    roles_id   int not null
)
    engine = MyISAM;

create index FK3say4n5o70d83i842ncv2l56x
    on usuarios_roles (roles_id);

create index FKqcxu02bqipxpr7cjyj9dmhwec
    on usuarios_roles (usuario_id);




INSERT INTO roles (id, nombre) VALUES (1, 'ADMIN');
INSERT INTO roles (id, nombre) VALUES (2, 'USER');


INSERT INTO usuarios (id, celular, correo, identificacion, nombre, password, provider, activo, usuario, provider_id) VALUES (1, '3007413871', 'jesusquinton@hotmail.com', '1002182720', 'Jesus Quinto', '$2a$10$u.X8OlkKhbmjhGMAeWrpt.T6icArwRYptTWJhz/WxZD2sjUG2z2xy', 1, 1, 'j5dev', null);
INSERT INTO usuarios (id, celular, correo, identificacion, nombre, password, provider, activo, usuario, provider_id) VALUES (4, '3007413871', 'vsgsgsg@sfsf.co', '3434553535', 'Andres ramirez', '$2a$10$u.X8OlkKhbmjhGMAeWrpt.T6icArwRYptTWJhz/WxZD2sjUG2z2xy', 1, 1, 'tubaraa', null);
INSERT INTO usuarios (id, celular, correo, identificacion, nombre, password, provider, activo, usuario, provider_id) VALUES (5, '3007413569', 'juanandres22@gmail.com', '100218272', 'Juan Andre', '$2a$10$u.X8OlkKhbmjhGMAeWrpt.T6icArwRYptTWJhz/WxZD2sjUG2z2xy', 1, 1, 'juan22', null);
INSERT INTO usuarios (id, celular, correo, identificacion, nombre, password, provider, activo, usuario, provider_id) VALUES (11, '3016916232', 'alberto@gmail.com', '12345678999', 'alberto gutierrez', '$2a$10$u.X8OlkKhbmjhGMAeWrpt.T6icArwRYptTWJhz/WxZD2sjUG2z2xy', 1, 1, 'alberto', null);
INSERT INTO usuarios (id, celular, correo, identificacion, nombre, password, provider, activo, usuario, provider_id) VALUES (10, '3007413871', 'andresp@gfjdfkg.co', '1002182725', 'Andres ramirez', '$2a$10$u.X8OlkKhbmjhGMAeWrpt.T6icArwRYptTWJhz/WxZD2sjUG2z2xy', 1, 1, 'ades24', null);

INSERT INTO usuarios_roles (usuario_id, roles_id) VALUES (1, 1);
INSERT INTO usuarios_roles (usuario_id, roles_id) VALUES (4, 2);
INSERT INTO usuarios_roles (usuario_id, roles_id) VALUES (5, 2);
INSERT INTO usuarios_roles (usuario_id, roles_id) VALUES (10, 2);
INSERT INTO usuarios_roles (usuario_id, roles_id) VALUES (11, 2);
